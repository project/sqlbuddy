<?php
/*

SQL Buddy - Web based MySQL administration
http://www.sqlbuddy.com/

index.inc
- output page structure, content loaded through ajax

MIT license

2008 Calvin Lough <http://calv.in>

*/

include "functions.inc";

loginCheck(false);

outputPage();

?>